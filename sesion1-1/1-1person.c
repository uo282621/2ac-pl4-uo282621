#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;
int main(int argc, char* argv[])
{
    Person Peter;
    PPerson pPeter;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;

    // TODO: Assign the weight
    Peter.weightkg = 70;
    // TODO: Show the information of the Peter data structure on the screen
    printf(" %s's height: %dcm; %s's weight: %fkg", Peter.name, Peter.heightcm,Peter.name, Peter.weightkg);
    return 0;
}
